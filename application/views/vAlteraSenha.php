<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="theme.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/vAlteraSenha.css')?>" type="text/css">
</head>

<body class="bg-dark">
  <div class="col-md-10 text-light" id="error">
              <?php echo validation_errors(); ?>
          </div>
  <div class="py-5 text-white opaque-overlay" style="">
    <div class="container">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <h1 class="text-gray-dark">Alterar Senha</h1>
          <br>
          <form class="" method="post" action="<?php echo base_url('usuario/alterarsenha')?>">
            <div class="form-group"> <label>Email</label>
              <input type="email" name="email" class="form-control" value="<?php if(isset($usuario->email)){echo $usuario->email;} ?>" placeholder="Email"> </div>
            <div class="form-group"> <label>Lembrete da Senha</label>
              <input type="text" name="lembrete_senha" class="form-control" value="<?php if(isset($usuario->lembrete_senha)){echo $usuario->lembrete_senha;} ?>" placeholder="Lembrete da Senha"> </div>
            <div class="form-group"> <label>Nova Senha</label>
              <input type="password" name="newsenha" class="form-control" value="<?php if(isset($newsenha)){echo $newsenha;} ?>" placeholder="Nova Senha"> </div>
            <div class="form-group"> <label>Confimar</label>
              <input type="password" name="senha" class="form-control" value="<?php if(isset($senha)){echo $senha;} ?>" placeholder="Confirmar Senha"> </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
            <a class="btn mx-3 btn-danger" href="javascript: history.back()">Cancelar</a>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
              <p>Enviado com Sucesso!.</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>

  </div>
</div>
</div>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

   <?php
        if($this->session->flashdata('enviado') == 'sucesso'){
            ?>
            <script type="text/javascript">
                $('#myModal').modal('show');
                $('#myModal').on('hidden.bs.modal', function () {
                 location.href = "<?php echo base_url('login/index') ?>";
                });
            </script>
        <?php
        }
        ?>
  <?php
        if($this->session->flashdata('msgLogin') == 'falha'){
    ?>
        <script language='javascript' type="text/javascript">
          $( "#login_Form" ).submit(function( event ) {
            alert( "Email ou senha estão errados!!" );
            event.preventDefault();
          });
   </script>

   <?php
      }
  ?>
</body>


</html>