<p class="display-4 my-4 text-center">Lista de Usuários</p>
<div class="container">
	<table class="table table-inverse">
	  <thead>
	    <tr>
	      <th>#</th>
	      <th>Nome</th>
	      <th>Email</th>
	      <th>Tipo Acesso</th>
	      <th>Modificar</th>
	    </tr>
	  </thead>
	   <tbody>
	   		<!-- Implementação da lista de usuários: tipo de acesso, botão alterar e deletar. Obs: Somente o master visualiza - RENATO -->
            <?php
            	$tipo1 = " USUÁRIO COMUM ";
            	$tipo2 = " MODERADOR ";
            	$tipo3 = " MASTER ";
                 foreach ($resultado as $linha): ?>
                <tr> 
                    <td><?php echo $linha->id; ?></td>
                    <td><a href="#"><?php echo $linha->nome; ?></a></td>
                    <td><?php echo $linha->email; ?></td>
                    <td>
                    	<?php if($linha->id_acesso == 1) { ?>
                    		<?php echo $tipo1; ?>
                    	<?php }else if($linha->id_acesso == 2){ ?>
                    		<?php echo $tipo2; ?>
                    	<?php }else{  ?>
                    		<?php echo $tipo3; ?>
                    	<?php } ?>
                    </td>
                    <td>
                    	<a class="btn mx-3 btn-light" href="<?php echo base_url('usuario/mostrarAcesso/'.$this->mAlteraID->encodeID($linha->id))?>    "> Alterar</a>
                    	<a class="btn mx-3 btn-danger" name="deletar" data-toggle="modal" data-target="#confirma-deletar" data-href="<?php echo base_url('usuario/deletarUsuario/'.$this->mAlteraID->encodeID($linha->id))?>    "> Deletar</a>
                    </td>
                    
                </tr>
            <?php endforeach; ?>

      	</tbody>
	</table>
</div>

<div class="modal fade" id="confirma-deletar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Alerta</h4>
            </div>

            <div class="modal-body">
                <p>Você realmente deseja deletar este registro?</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok">Sim, quero deletar!</a>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

 <script type="text/javascript">
        $('#confirma-deletar').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
</script>
   