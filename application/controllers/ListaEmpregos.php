<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ListaEmpregos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mListaEmpregos');
        $this->load->library('session');

    }
    public function index(){

        $infs = Array('viewProjeto'=>'vListaEmpregos',
            'noFooter'=>'', 'emprego' => $this->mListaEmpregos->lista($this->input->post('pesquisar')));
        $this->load->view('template',$infs);

    }
}