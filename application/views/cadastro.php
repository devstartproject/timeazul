<?php if($this->session->userdata('tipo_login') < 1) { ?>
<div class="row">
    <div class="Cadastro negrito">Junte-se a nos&nbsp;</div>
</div>
<div class="row">
    <div class="fontcadastro negrito">Faça parte da nossa equipe&nbsp;</div>
</div>
<br>
<br>
<br>
<div class="container xx  ">
    <div class="row ">
         <div class="col-md-10" id="error">
              <?php if(isset($mensagens)) echo $mensagens; ?>
          </div>
      <div class="col-md-10 ">
          <form class="teste" method="post" onsubmit="validacao();" action="<?php echo base_url('usuario/setCadastrar')?>">
            <div class="form-group"> <label>Nome Completo</label>
              <input type="text" name="nome" value="<?php if(isset($usuario->nome)){echo $usuario->nome;} ?>" class="form-control" placeholder=""> 
            </div>
            <div class="form-group"> <label>Email</label>
              <input type="" name="email" value="<?php if(isset($usuario->email)){echo $usuario->email;} ?>" class="form-control" placeholder=""> 
            </div>
            <div class="form-group"> <label>Senha</label>
              <input type="password" name="senha" value="<?php if(isset($senha)){echo $senha;} ?>" class="form-control" placeholder=""> 
            </div>
            <div class="form-group"> <label>Lembrete da Senha</label>
              <input type="text" name="lembrete_senha" value="<?php if(isset($lembrete_senha)){echo $lembrete_senha;} ?>" class="form-control" placeholder=""> 
            </div>
            <div class="form-group"> <label>Instituição de Ensino / Empresa</label>
              <input type="text" name="instituicao" value="<?php if(isset($usuario->instituicao)){echo $usuario->instituicao;} ?>" class="form-control" placeholder=""> 
            </div>
            <div style="text-align: center;">
              <button class="btn btn-primary" type="submit">Cadastrar
                <br>
              </button>
              <button class="btn btn-default" type="reset">Cancelar
                <br>
              </button>
            </div>
          </form>
      </div>
    </div>
<?php } ?>  
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
              <p>Enviado com Sucesso!.</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>

  </div>
</div>
</div>

<?php if($this->session->userdata('tipo_login') == 3) { ?>
        <br>
        <br>
        <div class="container xx  ">
           <div class="row ">
            <div class="col-md-10 ">
              <form class="teste" method="post" action="<?php echo base_url('usuario/atualizaAcesso')?>">
                <div class="form-group"> <label>Nível de Acesso Atual</label>
                      <input type="hidden" name="id_acesso" value="<?php if(isset($id_usuario)){echo $id_usuario;} ?>" class="form-control" placeholder=""> 
                  </div>
                <select class="form-control" id="acesso" name="nivelAcesso">
                    <option value="-1">---- Selecionar ----</option>
                    <option value="1">USUÁRIO COMUM</option>
                    <option value="2">MODERADOR</option>
                    <option value="3">MASTER</option>
                  </select>
                  <br>
                  <div style="text-align: center;">
                    <button class="btn btn-primary" id="btn2" type="submit">Atualizar
                      <br>
                    </button>
                    <br>
                  </div>
                </form>
              </div>
            </div>
          </div>
    <?php } ?>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>


 <?php
        if($this->session->flashdata('enviado') == 'sucesso'){
            ?>
            <script type="text/javascript">
                $('#myModal').modal('show');
                $('#myModal').on('hidden.bs.modal', function () {
                 location.href = "<?php echo base_url('eventos/index') ?>";
                });
            </script>
        <?php
        }
        ?>

   

   