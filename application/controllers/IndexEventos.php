<?php

//-------- Controller de cadastro de eventos - RENATO ---------//

defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller {

	//-------- Função carregar view - RENATO ---------//

	public function index()
	{
		$infs = Array('viewProjeto'=>'eventos'
					 );
		$this->load->view('template',$infs);
	}
}
