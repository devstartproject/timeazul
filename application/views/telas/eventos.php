  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Procurar eventos"> <span class="input-group-btn">
        <button class="btn btn-secondary" type="button">Go!</button>
      </span> </div>
        </div>
        <div class="col-md-4">
          <div class="btn-group">
            <button class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown"> Todas as Datas </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Amanhã</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Próxima Semana</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="col-lg-12"> </div>
  </div>
  <div class="py-4">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <img class="d-block mb-3 w-50 mx-auto img-fluid h-60" src="assets/img/banner.jpg">
              <div class="container">
                <div class="row">
                  <div class="col-md-3">
                    <div class="text-center date-body" style="width:80px"> <label for="" class="date-title">Setembro</label>
                      <div class="date-content">
                        <p class="dia">27</p>
                        <hr class="nomargin">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <h1 class="title-event">Feira Tecnologica Senai-PR</h1>
                  </div>
                </div>
                <div class="container">
                  <div class="row col-lg-3"> </div>
                </div>
              </div>
              <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
            <div class="col-md-6">
              <img class="d-block mb-3 w-50 mx-auto rounded img-fluid h-60" src="assets/img/banner_2.jpg">
              <div class="container">
                <div class="row">
                  <div class="col-md-3">
                    <div class="text-center date-body" style="width:80px"> <label for="" class="date-title">Setembro</label>
                      <div class="date-content">
                        <p class="dia">27</p>
                        <hr class="nomargin">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <h1 class="title-event">Evento dos eventos com eventos falando sobre eventos</h1>
                  </div>
                </div>
                <div class="container">
                  <div class="row col-lg-3"> </div>
                </div>
              </div>
              <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 my-4">
              <img class="img-fluid d-block mb-3 rounded w-50 mx-auto h-60" src="assets/img/banner_3.jpg">
              <div class="container">
                <div class="row">
                  <div class="col-md-3">
                    <div class="text-center date-body" style="width:80px"> <label for="" class="date-title">Setembro</label>
                      <div class="date-content">
                        <p class="dia">27</p>
                        <hr class="nomargin">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <h1 class="title-event">Evento dos eventos com eventos falando sobre eventos</h1>
                  </div>
                </div>
                <div class="container">
                  <div class="row col-lg-3"> </div>
                </div>
              </div>
              <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</p>
            </div>
            <div class="col-md-6 my-4">
              <img class="img-fluid d-block mb-3 rounded w-50 mx-auto h-60" src="assets/img/banner_3.jpg">
              <div class="container">
                <div class="row">
                  <div class="col-md-3">
                    <div class="text-center date-body" style="width:80px"> <label for="" class="date-title">Setembro</label>
                      <div class="date-content">
                        <p class="dia">27</p>
                        <hr class="nomargin">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <h1 class="title-event">Evento dos eventos com eventos falando sobre eventos</h1>
                  </div>
                </div>
                <div class="container">
                  <div class="row col-lg-3"> </div>
                </div>
              </div>
              <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="text-center py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <a href="#" class="btn border border-dark btn-outline-secondary" style="width: 200px">Carregar Mais</a>
        </div>
      </div>
    </div>
  </div>