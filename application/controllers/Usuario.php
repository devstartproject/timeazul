<?php

//-------- Controller de cadastro de usuários no sistema. Obs: feito pra cadastrar usuários comum posteriormente tornado-os master ou moderador - RENATO ---------//

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	 public  function __construct(){
	        parent::__construct();
	       	$this->load->model('mCadastro');
	       	$this->load->model('mLogin');
       		$this->load->library('session');
        }
	 
	public function cadastro()
	{
		$infs = Array('viewProjeto'=>'cadastro',
			'noHeader'=>'',
			'noFooter'=>'');
		$this->load->view('template',$infs);
	}

	public function setCadastrar(){

		//-------- Função de validação de informações do cadastro - RENATO ---------//

		$this->load->library('form_validation');
        $this->form_validation->set_rules('nome', 'Nome', 'required', array('required' => 'Você deve preencher o %s.'));	
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[usuario.email]', array('required' => 'Você deve preencher a %s.', 
        	'is_unique'=> 'Esse %s já está cadastrado.'));
        $this->form_validation->set_rules('senha', 'Senha', 'required', array('required' => 'Você deve preencher a %s.'));
        $this->form_validation->set_rules('lembrete_senha', 'Lembrete da Senha', 'required|max_length[20]', array('required' => 'Você deve preencher o %s.'));
        if($this->form_validation->run() == false){
           	$infs = Array('viewProjeto'=>'cadastro',
			'noHeader'=>'',
			'noFooter'=>'','mensagens' => validation_errors() );
			$this->load->view('template',$infs);
		}
        else{
        	$this->mCadastro->setCadastrar();
            $this->session->set_flashdata('enviado', 'sucesso');
            $this->cadastro();
         }
           	
	}

	public function listaUsuarios(){

		//-------- Função para listar usuários na view - RENATO ---------//

        $infs = Array('viewProjeto'=>'listaUsuarios',
			'noFooter'=>'', 'resultado' => $this->mCadastro->listaUsuarios());
        $this->load->view('template',$infs);
    	
	}

	public function mostrarAcesso($id){
        $this->load->helper('form');
        $id = $this->mAlteraID->decodeID($id);
        $consulta = $this->mCadastro->retorna_todos_registros($id);
        if(count($consulta)) {
                $consulta['usuario'] = $consulta[0];
            }
        $infs = Array('viewProjeto'=>'cadastro',
			'noFooter'=>'', 
			'usuario'=>$consulta[0],
			'id_usuario'=>$id);
		$this->load->view('template',$infs);
       //$this->load->view('cadastro',$consulta);
	}

	public function atualizaAcesso(){
		//-------- Função para atualizar acesso de usuários - RENATO ---------//
        $this->load->helper('form');

        $id = $this->input->post('id_acesso');
        $nivelAcesso = $this->input->post('nivelAcesso');
        
        $this->mCadastro->editarAcesso($id,$nivelAcesso);

        $infs = Array('viewProjeto'=>'cadastro',
			'noFooter'=>'', 'usuario'=>$consulta[0]);
		$this->load->view('template',$infs);
		redirect('Usuario/listaUsuarios');
       //$this->load->view('cadastro',$consulta);
	}

	public function deletarUsuario($id=null){
		//-------- deletar usuários - RENATO ---------//
            if($id != null) {
                $this->load->model('mCadastro');
                $id = $this->mAlteraID->decodeID($id);
                $dadosDelete['id'] = $id;
                $retorno = $this->mCadastro->deletarUsuario($dadosDelete);;
			    if($retorno==true){
			        $this->session->set_flashdata('sucesso', 'Registro deletado com sucesso!');
			    }else{
			        $this->session->set_flashdata('error', 'Erro ao deletar registro!');
			    }
			    echo redirect(base_url("Usuario/listaUsuarios"));
            }
        }

	public function alterarsenha($email=null){
		//--------alterar senha - RENATO ---------//
		$this->form_validation->set_rules('email', 'Email', 'required', array('required' => 'Você deve preencher a %s.'));
		$this->form_validation->set_rules('lembrete_senha', 'Lembrete da Senha', 'required|max_length[20]', array('required' => 'Você deve preencher a %s.'));
		$this->form_validation->set_rules('newsenha', 'Nova Senha', 'required', array('required' => 'Você deve preencher a %s.'));
		$this->form_validation->set_rules('senha', 'Senha', 'required|matches[newsenha]', array('required' => 'Você deve preencher a %s.'));
		$infs = Array('mensagens' =>'');
		
		if($this->form_validation->run() == false){
           	
			$this->load->view('vAlteraSenha',$infs);
		}else{
			$email = $this->input->post('email');
			$senha = $this->input->post('senha');
			$this->mCadastro->atualizasenha($email,$senha);
            $this->session->set_flashdata('enviado', 'sucesso');
            echo redirect(base_url("login/index"));
		}
	}
}
/* if($this->input->post()){
            
            $email = $this->input->post('email');
            $senha = $this->input->post('senha');
            $novasenha = $this->input->post('newsenha');

            if ($novasenha =! $senha) {
            	$this->session->set_flashdata('msgLogin', 'falha');
                return;
            }else{
            	$retorno = $this->mCadastro->verificaEmail($email);
            	if($retorno[0]->quantidade == 1){
            			$this->mCadastro->atualizasenha($email,$senha);
	            		$this->session->set_flashdata('enviado', 'sucesso');
	            		$this->load->view('vAlteraSenha',$infs);
	            }else{
	                $this->session->set_flashdata('msgLogin', 'falha');
	                $this->load->view('vAlteraSenha',$infs);
            	}
            }
	            
            

            
        }*/

		