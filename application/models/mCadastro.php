<?php 
	defined('BASEPATH') or exit('No direct access allowed');

	class mCadastro extends CI_MODEL{
		public function setCadastrar(){
            $dados = array (
                "nome" => $this->input->post("nome"),
                "email" => $this->input->post("email"),
                "senha" => md5($this->input->post("senha")),
                "lembrete_senha" => $this->input->post("lembrete_senha"),
                "instituicao" => $this->input->post("instituicao")
            );
            $query = $this->db->insert("usuario", $dados);
            if ($query){
                return true;
            }else{
                return false;
            }
        }

        public function listaUsuarios() {
                $this->db->select("id, nome, email, id_acesso");
                $query = $this->db->get("usuario")->result();
                return $query;
            }   
        
        public function retorna_todos_registros($id = 0){
             return $this->db->select('*')
                ->from('usuario')
                ->where('id',$id)
                ->get()
                ->result();
            }

        public function editarAcesso($id = 0,$nivelAcesso){
                $this->db->where("id", $id)
                                ->update("usuario", array('id_acesso'=>$nivelAcesso));
               
        }

        public function deletarUsuario($dados = null){
            if($dados != null){
                $this->db->delete('usuario',$dados);
            }
        }

        public function verificaEmail($email = null){
            return $this->db->select('count(id) as quantidade, email')
                ->where('email',$email)
                ->get('usuario')
                ->result();
        }

        public function atualizasenha($email = null,$senha){
                $this->db->where("email", $email)
                                ->update("usuario", array('senha'=>md5($senha)));
               
        }
                
            
    }
?>

