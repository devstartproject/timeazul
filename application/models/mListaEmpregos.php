<?php

defined('BASEPATH') OR
exit('Não é possivel acessar o arquivo deste modo!');
class mListaEmpregos extends CI_Model
{

    public function lista($pesquisar  = null) {
        $this->db->select("nome_vaga, empresa, descricao, salario, dt_publicacao");
        if($pesquisar != null){
            $this->db->or_where('nome_vaga like','%'.$pesquisar.'%');
            $this->db->or_where('empresa like','%'.$pesquisar.'%');
        }
        $this->db->from("emprego");
        return $this->db->get()->result();
    }
     public function deletaEmprego($id=null){
        $this->db->where('id',$id);
        $this->db->update('status', array('status', 0));

    }
 //   public function get_emprego()
    //   {
    //  $pesq = $this->input->post('pesquisar');
    //  $this->db->select('*');
    //  $this->db->like('nome_vaga',$pesq);
    //  return $this->db->get('emprego')->result();
    //}

}

