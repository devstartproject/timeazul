<?php

defined('BASEPATH') OR
exit('Não é possivel acessar o arquivo deste modo!');
class mListaCursos extends CI_Model
{

    public function lista($pesquisar  = null) {
        $this->db->select("nome, descricao, carga_horaria, preco, instituicao");
        if($pesquisar != null){
            $this->db->or_where('nome like','%'.$pesquisar.'%');
            $this->db->or_where('instituicao','%'.$pesquisar.'%');
        }
        $this->db->from("curso");
        return $this->db->get()->result();
    }



    // public function get_curso()
    //{
    //  $pesq = $this->input->post('pesquisar');
    //  $this->db->select('*');
    //  $this->db->like('nome',$pesq);
    //  return $this->db->get('curso')->result();
        //}

}
