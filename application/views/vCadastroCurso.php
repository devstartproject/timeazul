<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> </head>

<body>
<div class="container">
    <form class="" method="post" action="<?php echo base_url('Curso/setCadastrar')?>">
        <div class="form-group"> <label>Curso</label>
            <input type="text" name="nome" value="<?php if(isset($nome)){echo $nome;} ?>" class="form-control" placeholder=""> </div>
        <div class="form-group"> <label>Descrição</label>
            <input type="text" name="descricao" value="<?php if(isset($descricao)){echo $descricao;} ?>"  class="form-control" placeholder=""> </div>
        <div class="form-group"> <label>Carga Horaria</label>
            <input type="text" name="carga_horaria" value="<?php if(isset($carga_horaria)){echo $carga_horaria;} ?>"  class="form-control" placeholder=""> </div>
        <div class="form-group"> <label>Preço</label>
            <input type="text" name="preco" value="<?php if(isset($preco)){echo $preco;} ?>"  class="form-control" placeholder=""> </div>
        <div class="form-group"> <label>Instituição</label>
            <input type="text" name="instituicao" value="<?php if(isset($instituicao)){echo $instituicao;} ?>"  class="form-control" placeholder=""> </div>
        <input type="submit" class="btn text-right btn-success text-dark" value="Cadastrar"/>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>

</html>