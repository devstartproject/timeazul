<h1 class="display-1 text-center bg-success text-light">Notícias</h1>
<div class="container">
    <div class="row resumos">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <h1 class="">Heading</h1>
                    <img class="img-fluid d-block" src="<?php echo base_url('assets/img/bill-gates.jpg')?>">
                    <!-- <p class=""> <?php //echo $teste;?></p> -->
                    <p>Curabitur id massa sit amet mi ultrices suscipit. Praesent scelerisque in enim vel dignissim. Maecenas id ultrices enim. Proin dolor lectus, maximus vitae mauris in, vehicula bibendum risus. Duis interdum at massa ut pellentesque. Duis ligula
                        leo, imperdiet ut sapien eu, semper condimentum justo. Morbi blandit dolor sit amet mi interdum, in semper libero posuere. Sed massa urna, ultricies a ante nec, </p>
                    <a class="btn btn-N1 text-success"
                       href="">LEIA MAIS </a>
                </div>
                <div class="col-md-4">
                     <h1 class="">Heading</h1>
                    <img class="img-fluid d-block" src="<?php echo base_url('assets/img/amazon-echo-show.jpg')?>">
                    <p>Curabitur id massa sit amet mi ultrices suscipit. Praesent scelerisque in enim vel dignissim. Maecenas id ultrices enim. Proin dolor lectus, maximus vitae mauris in, vehicula bibendum risus. Duis interdum at massa ut pellentesque. Duis ligula
                        leo, imperdiet ut sapien eu, semper condimentum justo. Morbi blandit dolor sit amet mi interdum, in semper libero posuere. Sed massa urna, ultricies a ante nec, </p>
                    <a class="btn-N2 text-success" href="">LEIA MAIS </a>
                </div>
                <div class="col-md-4">
                    <h1 class="">Heading</h1>
                    <img class="img-fluid d-block" src="<?php echo base_url('assets/img/27151510581157-t1200x480.jpg')?>">
                    <p>Curabitur id massa sit amet mi ultrices suscipit. Praesent scelerisque in enim vel dignissim. Maecenas id ultrices enim. Proin dolor lectus, maximus vitae mauris in, vehicula bibendum risus. Duis interdum at massa ut pellentesque. Duis ligula
                        leo, imperdiet ut sapien eu, semper condimentum justo. Morbi blandit dolor sit amet mi interdum, in semper libero posuere. Sed massa urna, ultricies a ante nec, </p>
                    <a class="btn-N3 fre text-success" href="">LEIA MAIS </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h1 class="">Heading</h1>
                    <img class="img-fluid d-block " src="<?php echo base_url('assets/img/2017-01-17t234707z-1-mtzgrqed1hy7eno5-rtrfipp-0-facebook-tax.jpg')?>">
                    <p>Curabitur id massa sit amet mi ultrices suscipit. Praesent scelerisque in enim vel dignissim. Maecenas id ultrices enim. Proin dolor lectus, maximus vitae mauris in, vehicula bibendum risus. Duis interdum at massa ut pellentesque. Duis ligula
                        leo, imperdiet ut sapien eu, semper condimentum justo. Morbi blandit dolor sit amet mi interdum, in semper libero posuere. Sed massa urna, ultricies a ante nec, </p>
                    <a class="btn-N4 text-success" href="">LEIA MAIS </a>
                </div>
                <div class="col-md-4">
                    <h1 class="">Heading</h1>
                    <img class="img-fluid d-block" src="<?php echo base_url('assets/img/26201136043000-t1200x480.jpg')?>">
                    <p>Curabitur id massa sit amet mi ultrices suscipit. Praesent scelerisque in enim vel dignissim. Maecenas id ultrices enim. Proin dolor lectus, maximus vitae mauris in, vehicula bibendum risus. Duis interdum at massa ut pellentesque. Duis ligula
                        leo, imperdiet ut sapien eu, semper condimentum justo. Morbi blandit dolor sit amet mi interdum, in semper libero posuere. Sed massa urna, ultricies a ante nec, </p>
                    <a class="btn-N5 text-success" href="">LEIA MAIS </a>
                </div>
                <div class="col-md-4">
                    <h1 class="">Heading</h1>
                    <img class="img-fluid d-block" src="<?php echo base_url('assets/img/google.jpg')?>">
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                        aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a class="btn-N6 text-success"
                       href="">LEIA MAIS </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h1 class="">Heading</h1>
                    <img class="img-fluid d-block w-100 mb-3 rounded" src="<?php echo base_url('assets/img/apple3.jpg')?>">
                    <p>Curabitur id massa sit amet mi ultrices suscipit. Praesent scelerisque in enim vel dignissim. Maecenas id ultrices enim. Proin dolor lectus, maximus vitae mauris in, vehicula bibendum risus. Duis interdum at massa ut pellentesque. Duis ligula
                        leo, imperdiet ut sapien eu, semper condimentum justo. Morbi blandit dolor sit amet mi interdum, in semper libero posuere. Sed massa urna, ultricies a ante nec, </p>
                    <a class="btn-MN01 text-success" href="">LEIA MAIS </a>
                </div>
                <div class="col-md-6">
                    <h1 class="">Heading</h1>
                    <img class="img-fluid d-block w-100 mb-3 rounded" src="<?php echo base_url('assets/img/google.jpg')?>">
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                        aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a class="btn-MN02 text-success"
                       href="">LEIA MAIS </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="container">
    <div class="row leiaMais">
        <div class="col-md-12">
            <a class="btn buttonLOAD text-light text-center" href="">Carregar Mais</a>
        </div>
    </div>
</div>
