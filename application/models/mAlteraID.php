<?php
//------ mudar ID na parte da URL  - Renato --- // 
defined('BASEPATH') or exit('No direct access allowed');
class mAlteraID extends CI_Model{
    function encodeID($parSifra = null){
        $parSifra = $this->encryption->encrypt($parSifra);
        $parSifra = str_replace('/','+alterar+',$parSifra);
        return $parSifra;
    }
    function decodeID($parSifra = null){

        $parSifra = str_replace('+alterar+','/',$parSifra);
        $parSifra = $this->encryption->decrypt($parSifra);
        return $parSifra;
    }
}