
<div class="py-2 text-center ">
    <div class="container py-5 ">
        <div class="row">
            <div class="col-md-12 ">
                <h1 class="display-3 mb-4">Vagas de Emprego</h1>
            </div>
        </div>
    </div>
</div>
<div class="py-5">
    <div class="container">
        <form action="<?= base_url() ?>ListaEmpregos" method="post">
        <div class="row"> </div>
        <div class="row ">

            <div class="col-lg-4"> <label for="txt"><b>Pesquisar por vaga/Empresa</b></label>

                <div class="input-group"> <span class="input-group-btn"></span>
                    <input type="text" name="pesquisar" class="form-control" placeholder="Search for..." id="oi"> </div>
            </div>

            <div class="col-md3 ">
                <button type="submit" class="btn btn-success margintop" >Pesquisar</button>
            </div>
            </form>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-offset-3 col-lg-12"> </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
<table class="table-bordered container-fluid">
    <thead>
        <tr>
            <th>Nome da Vaga</th>
            <th>Empresa</th>
            <th>Descrição</th>
            <th>Salario</th>
            <th>Data Publicação</th>
        </tr>

    </thead>
    <tbody>
    <?php
    foreach ($emprego    as $emp): ?>

        <tr>
            <td><?php echo $emp->nome_vaga; ?></td>
            <td><?php echo $emp->empresa; ?></td>
            <td><?php echo $emp->descricao; ?></td>
            <td><?php echo $emp->salario; ?></td>
            <td><?php echo $emp->dt_publicacao; ?></td>

            <td>
                <a class="btn mx-3 btn-light">deletar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
</div>


