<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller {
	public function __construct(){
            parent::__construct();
            $this->load->model('mLogin');
            $this->load->library('session');
    }

	public function index()
	{
		if ($this->session->userdata('tipo_login') >= 0){
			$infs = Array('viewProjeto'=>'eventos'
					 );
			$this->load->view('template',$infs);
		}
		
	}

}
