 <div class="text-white bg-dark">
    <div class="container">
      <div class="row">
        <div class="text-md-left text-center align-self-center my-5 col-md-7">
          <h1 class="display-1">Bem Vindo</h1>
          <p class="display-4">Plataforma para Desenvolvedores e Empresas</p>
          <div class="row mt-5">
            <div class="col-md-5 col-6">
              <a href="#"></a>
            </div>
            <div class="col-md-5 col-6">
              <a href="#"></a>
            </div>
          </div>
        </div>
        <div class="h-50 col-md-5">
          <img class="d-block w-100 mx-auto rounded-circle h-100" src="<?php echo base_url('assets/img/test.gif')?>"> </div>
      </div>
    </div>
  </div>
  <div class="py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1 class="text-center display-2">Conheça nossas Páginas</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-4 ">
          <a href="<?php echo base_url('Noticia/index'); ?>">
            <img class="img-fluid d-block principal" src="<?php echo base_url('assets/img/newspaper.png')?>">
          </a>
          <h1 class="text-center">Notícias</h1>
        </div>
        <div class="col-md-4 ">
          <a href="<?php echo base_url('curso/index'); ?>">
            <img class="img-fluid d-block principal" src="<?php echo base_url('assets/img/knowledge.png')?>">
          </a>
          <h1 class="text-center">Conhecimento</h1>
        </div>
        <div class="col-md-4 ">
          <a href="<?php echo base_url('emprego/listaEmpregos'); ?>">
            <img class="img-fluid d-block principal" src="<?php echo base_url('assets/img/oportunidades.png')?>">
          </a>
          <h1 class="text-center">Oportunidades</h1>
        </div>
      </div>
    </div>
  </div>
