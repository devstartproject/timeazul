<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Curso extends CI_Controller
{

    public  function __construct(){
        parent::__construct();
        $this->load->model('mCurso');
        $this->load->library('session');
    }

    public function index()
    {

    $infs = Array('viewProjeto' => 'vListaCurso');
    $this->load->view('template', $infs);
       // $this->load->view('vCadastroEmprego');


    }
    public function setCadastrar(){
        $dgo = array (
            "nome" => $this->input->post("nome"),
            "descricao" => $this->input->post("descricao"),
            "carga_horaria" => $this->input->post("carga_horaria"),
            "preco" => $this->input->post("preco"),
            "instituicao" => $this->input->post("instituicao"),
        );


        $this->mCurso->setCadastrar($dgo);
        $this->index();
    }

}
?>