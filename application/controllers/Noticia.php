<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticia extends CI_Controller
{

    public  function __construct(){
        parent::__construct();
        $this->load->model('mNoticia');
        $this->load->library('session');
    }

    public function index()
    {

        $infs = Array('viewProjeto' => 'vNoticia');
        $this->load->view('template', $infs);

    }
    public function setCadastrar(){
        $dados = array (
            "titulo" => $this->input->post("titulo"),
            "assunto" => $this->input->post("assunto"),
            "texto" => $this->input->post("texto"),
        );


        $this->mNoticia->setCadastrar($dados);
        $this->index();
    }
    public function deleta($id=0){
        $this->mNoticia->deletaNoticia($id);
    }



}
?>
