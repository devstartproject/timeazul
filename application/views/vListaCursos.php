
<div class="py-2 text-center ">
    <div class="container py-5 ">
        <div class="row">
            <div class="col-md-12 colorheader">
                <h1 class="display-3 mb-4">Cursos Disponíveis</h1>
            </div>
        </div>
    </div>
</div>
<div class="py-5">
    <div class="container">
        <form action="<?= base_url() ?>ListaCursos" method="post">
            <div class="row"> </div>
            <div class="row margenleffft">

                <div class="col-lg-4"> <label for="txt"><b>Pesquisar por curso/Instituição</b></label>

                    <div class="input-group"> <span class="input-group-btn"></span>
                        <input type="text" name="pesquisar" class="form-control" placeholder="Search for..." id="oi"> </div>
                </div>

                <div class="col-md3 margintop">
                    <button type="submit" class="btn btn-success margintop" >Pesquisar</button>
                </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-offset-3 col-lg-12"> </div>
    </div>
</div>
</div>
<div class="container">
    <div class="row">
        <table class="table-bordered container-fluid">
            <thead>
            <tr>
                <th>Nome do Curso</th>
                <th>Descrição</th>
                <th>Carga Horária</th>
                <th>Preço</th>
                <th>Instituição</th>
            </tr>

            </thead>
            <tbody>
            <?php
            foreach ($curso    as $cur): ?>

                <tr>
                    <td><?php echo $cur->nome; ?></td>
                    <td><?php echo $cur->descricao; ?></td>
                    <td><?php echo $cur->carga_horaria; ?></td>
                    <td><?php echo $cur->preco; ?></td>
                    <td><?php echo $cur->instituicao; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


