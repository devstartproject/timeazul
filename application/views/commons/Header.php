<!DOCTYPE html>
<html>

<!-- Header de visualização de menu. 
  HTML e CSS - RENATO
  Montado menu Dashboard para usuário Master - RENATO 
  Altera botão para sair quando usuário está logado - RENATO  --> 

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans|Press+Start+2P" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
  <link rel="stylesheet" href="<?php echo base_url('assets/css/header.css')?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/index.css')?>" type="text/css"> 
  <link rel="stylesheet" href="<?php echo base_url('assets/css/cadastro.css')?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/empregos.css')?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/noticia.css')?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/principal.css')?>" type="text/css">
</head>
<body>
  <?php if(!isset($noHeader)){ ?>
    
   <nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <div class="container">
      <a class="navbar-brand text-success" href="#">
        <img src="<?php echo base_url('assets/img/logo.jpg')?>" width="100" height="100" class="d-inline-block align-top" alt=""> </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <?php if($this->session->userdata('tipo_login') > 1) { ?>
          <li class="nav-item">
            <a class="nav-link text-success" href="#">Perfil</a>
          </li>
          <?php } ?>
           <li class="nav-item">
            <a class="nav-link text-success" href="<?php echo base_url('Noticia/index'); ?>">Notícias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-success" href="<?php echo base_url('emprego/listaEmpregos'); ?>">Empregos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-success" href="<?php echo base_url('eventos/index'); ?>">Eventos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-success" href="<?php echo base_url('curso/index'); ?>">Cursos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link text-success" href="#">Portifólios</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-success" href="#">Contato</a>
          </li>
          <?php if($this->session->userdata('tipo_login') == 3) { ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle text-light" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                DashBoard
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="<?php echo base_url('Usuario/listaUsuarios'); ?>">Usuários</a>
                <a class="dropdown-item" href="#">Alterar Notícias</a>
                <a class="dropdown-item" href="#">Alterar Publicações</a>
                <a class="dropdown-item" href="#">Alterar Eventos</a>
                <a class="dropdown-item" href="#">Alterar Empregos</a>
                <a class="dropdown-item" href="#">Alterar Cursos</a>
              </div>
          
            </li>
          <?php } ?>

          <?php if(($this->session->userdata('tipo_login') == 3) or ($this->session->userdata('tipo_login') == 2) ) { ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle text-light" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Conteúdo
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Adicionar Notícias</a>
                <a class="dropdown-item" href="#">Adicionar Publicações</a>
                <a class="dropdown-item" href="#">Adicionar Eventos</a>
                <a class="dropdown-item" href="#">Adicionar Empregos</a>
                <a class="dropdown-item" href="#">Adicionar Cursos</a>
              </div>
          
            </li>
          <?php } ?>

        </ul>
        <form class="form-inline my-2 my-lg-0">
          <?php if($this->session->userdata('tipo_login') >= 1) { ?>
              <a class="btn mx-3 btn-light" href="<?php echo base_url('login/sair')?>"><i class="fa d-inline fa fa-sign-out"></i> Sair</a>
          <?php }else { ?>
          <a class="btn mx-3 btn-light" href="<?php echo base_url('usuario/cadastro')?>"><i class="fa d-inline fa-lg fa-user-circle-o"></i> Cadastre-se</a>
          <a class="btn mx-3 btn-primary" href="<?php echo base_url('login/index')?>"><i i class="fa d-inline fa fa-sign-in"></i> Entrar</a>
          <?php } ?>
        </form>
      </div>
    </div>
  </nav>
    <?php } ?>