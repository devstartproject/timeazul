 <div class="py-5 gradient-overlay text-center bg-secondary">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <h1 class="text-light">Sobre o Evento</h1>
              <p class="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p class="lead">Data: 01/10/2017 - 10/10/2017</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p class="lead">Organizado por:
            <a href="#">Renato Eventos</a>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          <a class="btn text-center btn-success" href="">Realizar Inscrição</a>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <img class="d-block img-fluid mx-auto" src="banner.jpg"> </div>
      </div>
    </div>
  </div>
