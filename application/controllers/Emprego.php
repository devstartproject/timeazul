<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Emprego extends CI_Controller
{

    public  function __construct(){
        parent::__construct();
        $this->load->model('mEmprego');
        $this->load->model('mListaEmpregos');
        $this->load->library('session');
    }

    public function index()
    {

        $infs = Array('viewProjeto' => 'vCadastroEmprego');
        $this->load->view('template', $infs);

    //$this->load->view('vCadastroEmprego');

    }
    public function setCadastrar(){
        $xpto = array (
            "nome_vaga" => $this->input->post("nome_vaga"),
            "empresa" => $this->input->post("empresa"),
            "descricao" => $this->input->post("descricao"),
            "salario" => $this->input->post("salario"),
        );


        $this->mEmprego->setCadastrar($xpto);
        $this->index();
    }



    public function listaEmpregos(){
        $infs = Array('viewProjeto' => 'vListaEmpregos', 'emprego' => $this->mListaEmpregos->lista());
        $this->load->view('template', $infs);
    }

    public function deletarEmprego($id=null){
        //-------- deletar usuários - RENATO ---------//
        if($id != null) {
            $this->load->model('mEmprego');
            $id = $this->mAlteraID->decodeID($id);
            $dadosDelete['id'] = $id;
            $retorno = $this->mCadastro->deletarUsuario($dadosDelete);;
            if($retorno==true){
                $this->session->set_flashdata('sucesso', 'Registro deletado com sucesso!');
            }else{
                $this->session->set_flashdata('error', 'Erro ao deletar registro!');
            }
            echo redirect(base_url("Usuario/listaUsuarios"));
        }
    }
}
?>