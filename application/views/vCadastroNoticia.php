<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> </head>

<body>
<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-1 text-center bg-dark text-success">Cadastro Noticia</h1>
                <form class="" method="post" action="<?php echo base_url('Noticia/setCadastrar')?>">
                    <div class="form-group"> <label>Title</label>
                        <input type="" name="titulo" value="<?php if(isset($titulo)){echo $titulo;} ?>"  class="form-control" placeholder="title"> </div>
                    <div class="form-group"> <label>Categotia</label>
                        <input type="" name="assunto"value="<?php if(isset($assunto)){echo $assunto;} ?>" class="form-control" placeholder="subtile"> </div>
                <div class="form-group"> <textarea></textarea>
                        <input type="" name="texto"  value="<?php if(isset($texto)){echo $texto;} ?>" class="form-control" placeholder="">
                </div>
                    <input type="submit" class="btn text-right btn-success text-dark" value="Salvar"/>

                </form>
                <div class="row text-left">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a href="#" class="btn text-justify btn-success text-dark">Preview</a>
                            <a href="#" class="btn btn-success text-dark">Carregar Imagem</a>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/js/tinymce/tinymce.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tinymce/langs/pt_BR.js'); ?>"></script>
<script>tinymce.init({ selector:'textarea' });</script>

</head>
</body>

</html>