<?php

//-------- Controller de login: validação de usuários e controle de sessão - RENATO ---------//

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model('mLogin');
    }
	public function index()
	{
		$infs = Array('viewProjeto'=>'login',
					  'noHeader'=>'',
					  'noFooter'=>'');
		$this->load->view('login');
	}
	public function autenticar(){
        //-------- Função de autenticação e verificação de usuário - RENATO ---------//
        if($this->input->post()){
            
            $email = $this->input->post('email');
            $senha = $this->input->post('senha');

            $retorno = $this->mLogin->verificaLogin($email,$senha);

            if($retorno[0]->quantidade == 1){
                    // $this->session->set_flashdata('msgLogin', 'sucesso');
                    $infoUsuario = Array('tipo_login'=>$retorno[0]->tipo_login, 
                                        'id'=>$retorno[0]->id); 
                    $this->session->set_userdata($infoUsuario);
                    redirect('Eventos');
            }else{
                $this->session->set_flashdata('msgLogin', 'falha');
                $this->index();
            }
        }
    }

    public function sair(){
        $this->session->sess_destroy();
        
        redirect('login/index');
    }
}
