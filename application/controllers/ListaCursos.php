<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ListaCursos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mListaCursos');
        $this->load->library('session');

    }
    public function index(){

        $infs = Array('viewProjeto'=>'vListaCursos',
            'noFooter'=>'', 'curso' => $this->mListaCursos->lista($this->input->post('pesquisar')));
        $this->load->view('template',$infs);

    }
}