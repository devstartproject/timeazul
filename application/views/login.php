<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/login.css')?>" type="text/css"> 
</head>

<body class="w-100 h-100">
  <div class="h-100 py-5 my-5">
    <div class="container">
      <div class="row">
        <div class="col-md-3"> </div>
        <div class="col-md-6 my-5">
          <div class="card text-white bg-secondary px-5">
            <div class="card-body">
              <h1 class="mb-4">Login</h1>
              <form action="<?php echo base_url('Login/autenticar'); ?>" method="post" id="login_Form" class="form-signin">
                <div class="form-group"> <label>Email</label>
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email"> </div>
                  <span data-alertid="example"></span>
                <div class="form-group"> <label>Senha</label>
                  <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha"> </div>
                <button type="submit" class="btn-lg text-center btn-success w-100 m-1">Entrar</button>
              </form>
            </div>
            <p class="">
              <a class="text-white" href="<?php echo base_url('usuario/alterarsenha'); ?>">Esqueci minha senha</a>
            </p>
            <p class="">Não tem conta?
              <a href="<?php echo base_url('usuario/cadastro'); ?>" class="text-white">Cadastre-se aqui</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
   <?php
        if($this->session->flashdata('msgLogin') == 'falha'){
    ?>
        <script language='javascript' type="text/javascript">
          $( "#login_Form" ).submit(function( event ) {
            alert( "Email ou senha estão errados!!" );
            event.preventDefault();
          });
   </script>

   <?php
      }
  ?>
</body>

</html>